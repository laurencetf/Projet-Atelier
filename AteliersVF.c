#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#define couleur(param) printf("\033[%sm",param)

typedef struct{    //Définition de la structure de données pixel contenant les valeurs entières du rouge, du vert et du bleu
    int rouge;
    int vert;
    int bleu;
} pixel;


typedef struct{
	int largeur;
	int hauteur;
	pixel** tableau;
} image;

typedef struct{
	int comptage;
	int** bin;
} structCompt;


/*--------------------------------------------------------------
 --nom            : ouvertureFic
 --rôle           : Lire un fichier image et récupérer les données le concernant, c'est à dire la matrice de pixels représentant cette image, sa largeur et sa hauteur
 --paramètres     : Le nom d'un fichier image au format .ppm
 --retour         : Une variable de type image représentant l'image du fichier
 --auteur         : GAINCHE Thibault <gainchethi@eisti.eu> / LAURENCET Fabien <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 */
image ouvertureFic(FILE* fichier) {

	  	pixel** mat; //Déclaration de la matrice de pixel resultat
	  	image img;
       	int largeu;
       	int hauteu;
	    char commentaire[256]; 							// Déclaration d'une chaine de 256 chars maximums						// Déclaration d'une chaine de 16 chars maximums
	    char intensiteMax[4]; 							// Déclaration d'une chaine de 4 chars maximums

	    char largeur[5];								// Déclaration d'une chaine de 5 chars maximums
	    char hauteur[5];								// Déclaration d'une chaine de 5 chars maximums

	    fgets(commentaire, 256, fichier);				//Assignation de 256 chars maximums, correspondant à la deuxieme ligne du fichier, à la chaine commentaire

	    fscanf(fichier, "%s %s", largeur, hauteur);	//Récupération des dimensions dans la troisième ligne du fichier. On affecte le premier nombre correspondant a la largeur dans la variable "largeur", et le second nombre correspondant à la hauteur à la variable "hauteur"
	    fseek(fichier, 1, SEEK_CUR); 					//Positionnement du pointeur au début de la ligne suivante (Début de la quatrième ligne)
	    fgets(intensiteMax, 200, fichier);


	    int i = 0;		//Déclaration des itérateurs et d'un tableau qui contiendra les valeurs des composantes rouge verte et bleue d'un pixel
	    int j = 0;
	    int couleur[3];

	    largeu = atoi(largeur);  //Conversion des hauteur et largeur en integer
	    hauteu = atoi(hauteur);

	    mat = malloc((hauteu) * sizeof(pixel*)); //Allocation de mémoire pour la matrice résultat mat


	    while (i < hauteu)//	&& (fichier != NULL))
	    	{
				mat[i] = malloc((largeu) * sizeof(pixel));
				while (j < largeu)
					{
				    	fscanf(fichier,"%i\n%i\n%i\n", &couleur[0], &couleur[1], &couleur[2]);
				       	mat[i][j].rouge = couleur[0];
				    	mat[i][j].vert = couleur[1];
				    	mat[i][j].bleu = couleur[2];
				     	j++;

				  	};	
				j = 0;
				i++;
	      	}
	  
	img.tableau=mat;
	img.largeur=largeu;
	img.hauteur=hauteu;
	return img;
}




/*--------------------------------------------------------------
 --nom            : SaveBin
 --rôle           : Sauvegarde une image dans un fichier
 --paramètres     : Un tableau de 0 et de 1 représentant une image binarisée, l'image représentée par le tableau 
 					(pour accéder facilement à la hauteur et largeur du tableau) et le nom du fichier dans lequel sauvegegarder l'image 					
 --retour         : 1 si la sauvegarde s'est bien passée, 0 sinon
 --auteur         : GAINCHE Thibault <gainchethi@eisti.eu> / LAURENCET Fabien <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 */
int SaveBin(int ** tableau, image img ,char nom[20]) {
	int x;														//définition de l'abscisse
	int y;														//définition de l'ordonnée
   	int res;
   	int pix;
   	FILE* fichier =NULL;										//Procédure de vérification de l'ouverture d'un fichier
   	fichier=fopen(nom,"w+");									//On assigne la valeur NULL au fichier, on ouvre ensuite le fichier et on vérifie après si le fichier a toujours la valeur NULL
   	if (fichier == NULL) {
		res=0;													//On retourne 0 en cas de non ouverture du fichier
    }else {
   		fputs("P3\n",fichier);									//Écriture de l'entête (Type, commentaire, largeur, hauteur, le nombre de couleurs)
   		fputs("#Image créée par le projet©\n",fichier);
   		fprintf(fichier,"%d %d\n", img.largeur,img.hauteur);
   		fprintf(fichier, "%d", 255);
   		for (x=0 ; x< img.hauteur ; x++) {						//On parcourt le tableau de lignes en lignes
      		for (y=0 ; y < img.largeur ; y++) {
      			switch(tableau[x][y]){
      				case 0 : pix= 0;
      				break;
      				case 1 : pix= 255;
      				break;
      			}
        		fprintf(fichier,"\n%d\n%d\n%d",pix,pix,pix );	//On écrit pour chaque itération, les valeurs du rouge du vert et du bleus successivement
    		}
    	}
    	res =1;													//On retourne 1 lorsque le traitement est terminé
	}
	fclose(fichier);											//Fermeture du fichier
	return res;														
}


//Ancienne fonction de calcul du seuil
/*--------------------------------------------------------------
 --nom            : calculSeuil
 --rôle           : Calcule le seuil d'une image
 --paramètres     : Une variable de type image
 --retour         : Le seuil correspondant, qui correspond à l'écart type de l'image
 --auteur         : GAINCHE Thibault <gainchethi@eisti.eu> / LAURENCET Fabien <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 
float calculSeuil(image img) {
	int x;
	int y;
	float somme = 0;
	for (x = 0; x < img.hauteur; ++x){	//on parcourt le tableau ligne par ligne puis colonne par colonne
		for (y = 0; y < img.largeur; ++y){
							
   			somme += img.tableau[x][y].rouge;							//L'image est en niveaux de gris donc on calcule la somme de chaque valeur du pixel rouge (choix arbitraire)
   		}
   	}

   	float moyenne = somme / (img.hauteur * img.largeur);				//On divise la somme par le nombre de pixels pour obtenir la moyenne

   	float somme2 = 0;
   	for (x = 0; x < img.hauteur; ++x){
   		for (y = 0; y < img.largeur; ++y){
   			somme2 += pow(img.tableau[x][y].rouge - moyenne, 2);		//On calcule la somme des carrés des valeurs de chaque pixel moins la moyenne trouvée, afin de déterminer la variance
   		}
   	}
   	float variance = somme2 / (img.hauteur * img.largeur); 			//On divise cette nouvelle somme par le nombre de pixels pour trouver la variance
   	float ecartType = sqrt(variance);									//On calcule l'écart-type en prenant la racine carrée de la variance. L'écart type est la valeur du seuil.
	printf("ecType2 : %f\n",ecartType);
	return ecartType;

}
*/



/*--------------------------------------------------------------
 --nom            : binarise
 --rôle           : Binarise une image en noir et blanc en remplissant un tableau avec des 0 ou des 1
 --paramètres     : La valeur du seuil de binarisation et l'image img à binariser
 --résultat       : Le tableau d'entiers correspondant à la matrice de pixels binarisés
 --auteur         : GAINCHE Thibault <gainchethi@eisti.eu> / LAURENCET Fabien <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 */
int** binarise(float seuil, image img){
	int x;
	int y;
	int** resultat;

	resultat=malloc(img.hauteur * sizeof(int *));
   	for (x=0 ; x < img.hauteur ; ++x) {  //On parcourt le tableau de lignes en lignes
   		resultat[x]=malloc(img.largeur * sizeof(int));							
    	for (y=0 ; y < img.largeur ; ++y) {	 //On parcourt le tableau de colonnes en colonnes
            if (((img.tableau[x][y].rouge+img.tableau[x][y].vert+img.tableau[x][y].bleu)/3.0)>seuil) {	//Calcul de la moyenne du pixel et comparaison avec le seuil
            	resultat[x][y]=1;			//Assignation de la valeur 0 à la case d'abscisse x et d'ordonnée y du tableau d'entier résultat pour représenter le blanc
            } else {
            	resultat[x][y]=0;			//Assignation de la valeur 1 à la case d'abscisse x et d'ordonnée y du tableau d'entier résultat pour représenter le noir
            }
        }
    }
    return resultat;
}




/*--------------------------------------------------------------
 --nom            : delai
 --rôle           : Permet d'attendre qu'une action s'exécute (utilisée pour le débeugage et la vérification)
 --paramètres     : Un entier i représentant le nombre de secondes que l'on souhaite attendre
 --retour         : Aucun
 --auteur		  : GAINCHE Thibault <gainchethi@eisti.eu> / LAURENCET Fabien <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 */
void delai(int i)    /*Pause l'application pour i seconds*/
{
    clock_t start,end;
    start=clock();
    while(((end=clock())-start)<=i*CLOCKS_PER_SEC);
}




/*--------------------------------------------------------------
 --nom            : afficheTab
 --rôle           : Affiche sur le terminal la représentation de l'image grâce à un tableau 
 					de 0 et de 1 colorés (utilisée pour le débeugage et les tests)
 --paramètres     : un tableau de 0 et de 1 représentant une image binarisée
 --retour         : Aucun
 --auteur         : GAINCHE Thibault <gainchethi@eisti.eu> / LAURENCET Fabien <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 */
void afficheTab(int ** tab, image img){	//Permet d'afficher la simulation sous forme de Tableau dans le terminal
	int x;
	int y;
	printf("*** %d %d \n", img.largeur, img.hauteur);
	for (x = 0; x < img.hauteur; ++x){
		printf("|");
		for (y = 0; y < img.largeur; ++y){
			switch (tab[x][y]){
				case 0 : couleur("30;30");
						 printf("0");
						 couleur("30;30");
				break;
				case 1 : couleur("37;37");
						 printf("1");
						 couleur("37;37");
				break;
			}
		}
		printf("|\n");
	}
	printf("\n");
}




/*--------------------------------------------------------------
 --nom            : erase
 --rôle           : Pour chaque pixel autour du pixel [x][y], on regarde si il appartient à l'image.
 					Si il est blanc, on fait le même traitement sur celui-ci, jusqu'à ce que la tâche soit remplie
 --paramètres     : La matrice de 0 et de 1 représentant l'image, les coordonnées x et y du pixel blanc trouvé,
 					et l'image (pour accéder à la largeur et la hauteur)
 --retour         : La matrice de 0 et de 1 rerésentant l'image traitée
 --auteur         : GAINCHE Thibault <gainchethi@eisti.eu> / LAURENCET Fabien <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 */
int** erase(int ** tab,int x, int y, image img){
    tab[x][y]=0;

    if((x+1<img.hauteur)&&(tab[x+1][y]==1)){ //On teste chaque pixel autour du pixel blanc trouvé
        tab=erase(tab,x+1,y,img);			 //Appel récursif sur le pixel blanc trouvé autour de celui concerné
    }
    if((x-1>=0)&&(tab[x-1][y]==1)){
        tab=erase(tab,x-1,y,img);
    }
    if((y-1>=0)&&(tab[x][y-1]==1)){
        tab=erase(tab,x,y-1,img);
    }
    if((y+1<img.largeur)&&(tab[x][y+1]==1)){
        tab=erase(tab,x,y+1,img);
    }
    if((x-1>=0)&&(y-1>=0)&&(tab[x-1][y-1]==1)){
        tab=erase(tab,x-1,y-1,img);
    }
    if((x+1<img.hauteur)&&(y+1<img.largeur)&&(tab[x+1][y+1]==1)){
        tab=erase(tab,x+1,y+1,img);
    }
    if((x+1<img.hauteur)&&(y-1>=0)&&(tab[x+1][y-1]==1)){
        tab=erase(tab,x+1,y-1,img);
    }
    if((x-1>=0)&&(y+1>=0)&&(tab[x-1][y+1]==1)){
        tab=erase(tab,x-1,y+1,img);
    }
    return tab;
}


/*--------------------------------------------------------------
 --nom            : comptage
 --rôle           : Compter le nombre de tâches sur une image
 --paramètres     : Une variable de type image img, et le tab
 --retour         : Une variable de type strucCompt, qui contient le nombre de cellules comptées, ainsi que la matrice de pixel binarisée,
 					qui est en fin de comptage une matrice de 0 car toutes les taches on été effacées.
 --auteur         : GAINCHE Thibault <gainchethi@eisti.eu> / LAURENCET Fabien <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 */
structCompt comptage (image img, int** bina){
	int compt=0;
	int x;
	int y;
	structCompt res;
	for (x = 0; x < img.hauteur; ++x){ //On parcourt l'image 
		for (y = 0; y < img.largeur; ++y){
			if (bina[x][y]==1){ //Si on rencontre un pixel blanc, c'est une tache
				++compt;	//On incrémente le nombre de taches de 1
              	bina=erase(bina,x,y,img); //Puis on remplie la tache blanche par des pixels noirs pour la faire disparaitre
			}
		}
	}
	res.comptage=compt; 
	res.bin=bina;
	return res;
}



/*--------------------------------------------------------------
 --nom            : otsu
 --rôle           : Calcule le sueil de binarisation grâce à la méthode d'otsu
 --paramètres     : L'image dont on veut calculer le seuil
 --retour         : Le seuil de binarisation
 --auteur         : GAINCHE Thibault <gainchethi@eisti.eu> / LAURENCET Fabien <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 */
int otsu(image img) {
    
  	int hist[256];
 	double prob[256], omega[256]; 		//Probabilites des niveaux de gris
 	double mu[256];   /* mean value for separation */
 	double max_sigma, sigma[256]; 		//Variance interclasse
 	int i, x, y; 		//Variables de boucle
 	int seuil; 			//Seuil de binarisation recherché
  	int largeur = img.largeur;
  	int hauteur = img.hauteur;
  
  
 	for (i = 0; i < 256; i++) hist[i] = 0; 		//Initialisation de l'histogramme

 	for (x = 0; x < hauteur; x++) {  		//Création de l'histogramme
 	   	for (y = 0; y < largeur; y++) {
 	   		int valeurDeGris = img.tableau[x][y].rouge;
      		hist[valeurDeGris]++;
    	}
	}


  	for ( i = 0; i < 256; i ++ ) { 		//Calcul de la densité de probabilité
  		prob[i] = (double)hist[i] / (largeur * hauteur);
 	 }
  
  	omega[0] = prob[0]; 		//Initialisation de omega et mu
  	mu[0] = 0.0;       
  	for (i = 1; i < 256; i++) {		//Création des tableaux omega et mu
  		omega[i] = omega[i-1] + prob[i];
   		mu[i] = mu[i-1] + i*prob[i];
  	}
  
     //Sigma est la maximisation de la variance inter-classe, et permet de déterminer le seuil optimal

 	seuil = 0;		//Initialisation du seuil
  	max_sigma = 0.0;	
 	for (i = 0; i < 256-1; i++) { 		//Determination du seuil
    	if ((omega[i] != 0.0) && (omega[i] != 1.0)) {
      		sigma[i] = pow(mu[256-1]*omega[i] - mu[i], 2) / (omega[i]*(1.0 - omega[i]));
   		} else {
    		sigma[i] = 0.0;
    	}

    	if (sigma[i] > max_sigma) {
      		max_sigma = sigma[i];
      		seuil = i;
    	}
 	}  
      return seuil;

}



int main(){

	char nomImg[256];
	int res;
	system("clear");
	printf("\n\n\n\nVeuillez saisir le nom de l'image à traiter svp (n'oubliez pas de saisir l'extension .ppm) : ");
	scanf("%s", nomImg);
	printf("\n\n");

	FILE* fichier = NULL;
    fichier = fopen(nomImg, "r");
    
   	if (fichier == NULL) {
		printf("Fichier inexistant\n\n");
		res = 1;
	} else {

		char typeImg[64];								//Déclaration d'une chaine de 64 chars maximums
		fgets(typeImg, 64, fichier);					//Assignation de 64 chars maximums, correspondant à la premiere ligne du fichier, à la chaine typeImg
		char* typePpm = "P3\n";

		if (strcmp(typeImg, typePpm) == 0) { 			//Comparaison du type de fichier ppm universel avec celui du fichier entré

			image img= ouvertureFic(fichier); 							//Lecture de l'image et création de la variable de type image
			fclose(fichier);
			float seuil = otsu(img); 									//Calcul du seuil

			int** binarisedImg = binarise(seuil,img); 					//Binarisation de l'image
			//SaveBin(binarisedImg,img,"binarised.ppm"); 	//Sauvegarde l'image binarisée dans le fichier binarised.ppm

			structCompt resu = comptage(img,binarisedImg); 
			int nbCellules = resu.comptage;
			//int** imgFinale = resu.bin;
			//SaveBin(imgFinale,img,"final.ppm"); 			//Permet de sauvegarder l'image traitée dans le fichier final.ppm : 
															//elle doit être toute noire car toutes les tâches on été remplies

			printf("Le nombre de cellules présentes sur l'image est : %d\n\n",nbCellules);
			res = 0;
		} else {
			fclose(fichier);
			printf("Ce fichier n'est pas de type ppm. Veuillez sélectionner un fichier au format ppm valide s'il vous plaît.\n\n");
		}
	}
	return res;
}
