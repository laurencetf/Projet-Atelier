Pour lancer le programme :

1)Compilez le fichier source AteliersVF.c en saisissant dans le terminal la commande "gcc -o FO AteliersVF.c -lm", FO �tant le nom du fichier ex�cutable,
2)Lancez le programme avec la commande "./FO" FO �tant le nom du fichier ex�cutable pr�c�demment saisit,
3)Saisissez le nom de l'image dont vous souhaitez compter les cellules parmis les images propos�es dans l'archive,
4)V�rifiez le r�sultat.

Si vous souhaitez observer les r�sultats en enregistrant l'image binaris�e et l'image apr�s le traitement, veuillez supprimez les "//" aux lignes 413, 417 et 418.